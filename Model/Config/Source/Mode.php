<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Mode implements ArrayInterface
{
    public const MODE_VALUE_1 = 1;
    public const MODE_VALUE_2 = 2;
    public const MODE_VALUE_3 = 3;
    private const MODE_LABEL_1 = 'Mode 1: Changes the value of saved HTML in the database for cms page/block';
    private const MODE_LABEL_2 = 'Mode 2: Changes the value of the displayed HTML displayed on the storefront when the page is loaded';
    private const MODE_LABEL_3 = 'Mode 3: Changes the value of the displayed HTML for the entire page (all blocks/page together)';
    private const VALUE = 'value';
    private const LABEL = 'label';

    public function toOptionArray(): array
    {
        return [
            [self::VALUE => self::MODE_VALUE_1, self::LABEL => __(self::MODE_LABEL_1)],
            [self::VALUE => self::MODE_VALUE_2, self::LABEL => __(self::MODE_LABEL_2)],
            [self::VALUE => self::MODE_VALUE_3, self::LABEL => __(self::MODE_LABEL_3)]
        ];
    }

    public function toArray(): array
    {
        return [
            self::MODE_VALUE_1 => __(self::MODE_LABEL_1),
            self::MODE_VALUE_2 => __(self::MODE_LABEL_2),
            self::MODE_VALUE_3 => __(self::MODE_LABEL_3),
        ];
    }
}
