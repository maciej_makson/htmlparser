<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model;

use DOMXPath;

class Parser
{
    private const EXPRESSION = "//";
    const QUERY_EXPRESSION = '/';
    const INDEX = 0;
    const REPLACE_ARRAY = [
        '%7B' => '{',
        '%7D' => '}',
        '%20' => ' '
    ];

    private \DOMDocument $DOMDocument;

    private ConfigProvider $configProvider;

    public function __construct(\DOMDocument $document, ConfigProvider $configProvider)
    {
        $this->DOMDocument = $document;
        $this->configProvider = $configProvider;
    }

    public function parse(string $html): string
    {
        return $this->modifyHtml(
            $html,
            $this->configProvider->getTag(),
            $this->configProvider->getAttribute(),
            $this->configProvider->getValue()
        );
    }

    private function modifyHtml(string $html, string $tag, string $attribute, string $value): string
    {
        libxml_use_internal_errors(true);
        $this->DOMDocument->loadHTML($html);
        libxml_clear_errors();
        $document = new DOMXPath($this->DOMDocument);
        foreach ($document->query(self::EXPRESSION . $tag) as $node) {
            $node->setAttribute($attribute, $value);
        }

        $str = $document->document->saveHTML(
            (new \DOMXPath($document->document))->query(self::QUERY_EXPRESSION)->item(self::INDEX)
        );

        return str_replace(array_keys(self::REPLACE_ARRAY), array_values(self::REPLACE_ARRAY), $str);
    }
}
