<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model\CMS;

use Alpacode\HtmlParser\Model\Parser;
use Alpacode\HtmlParser\Model\Voters\Mode;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Blocks
{
    private BlockRepositoryInterface $blockRepository;

    private SearchCriteriaBuilder $searchCriteriaBuilder;

    private Parser $parser;

    private Mode $mode;

    public function __construct(
        BlockRepositoryInterface $pageRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Parser $parser,
        Mode $mode
    ) {
        $this->blockRepository = $pageRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->parser = $parser;
        $this->mode = $mode;
    }

    private function getBlocks(): array
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();

        return $this->blockRepository->getList($searchCriteria)->getItems();
    }

    private function save($page): void
    {
        $this->blockRepository->save($page);
    }

    public function execute(): void
    {
        if (!$this->mode->getMode1()) {
            return;
        }

        foreach ($this->getBlocks() as $block) {
            $block->setContent($this->parser->parse($block->getContent()));
            $this->save($block);
        }
    }
}
