<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model\CMS;

use Alpacode\HtmlParser\Model\Voters\Mode;
use Alpacode\HtmlParser\Model\Parser;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Pages
{
    private PageRepositoryInterface $pageRepository;

    private SearchCriteriaBuilder $searchCriteriaBuilder;

    private Parser $parser;

    private Mode $mode;

    public function __construct(
        PageRepositoryInterface $pageRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Parser $parser,
        Mode $mode
    ) {
        $this->pageRepository = $pageRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->parser = $parser;
        $this->mode = $mode;
    }

    private function getPages(): array
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        return $this->pageRepository->getList($searchCriteria)->getItems();
    }

    private function save($page): void
    {
        $this->pageRepository->save($page);
    }

    public function execute(): void
    {
        if (!$this->mode->getMode1()) {
            return;
        }
        foreach ($this->getPages() as $page) {
            $page->setContent($this->parser->parse($page->getContent()));
            $this->save($page);
        }
    }
}
