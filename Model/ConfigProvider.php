<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class ConfigProvider extends AbstractHelper
{
    private const IS_ENABLED = 'alpacode/general/enable_module';
    private const TAG = 'alpacode/general/tag';
    private const ATTRIBUTE = 'alpacode/general/attribute';
    private const VALUE = 'alpacode/general/value';
    private const MODE = 'alpacode/general/parser_mode';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(Context $context, ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    public function getIsEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::IS_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    public function getMode(): ?int
    {
        return (int)$this->scopeConfig->getValue(self::MODE, ScopeInterface::SCOPE_STORE);
    }

    public function getTag(): ?string
    {
        return $this->scopeConfig->getValue(self::TAG, ScopeInterface::SCOPE_STORE);
    }

    public function getAttribute(): ?string
    {
        return $this->scopeConfig->getValue(self::ATTRIBUTE, ScopeInterface::SCOPE_STORE);
    }

    public function getValue(): ?string
    {
        return $this->scopeConfig->getValue(self::VALUE, ScopeInterface::SCOPE_STORE);
    }
}
