<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Model\Voters;

use Alpacode\HtmlParser\Model\ConfigProvider;
use Alpacode\HtmlParser\Model\Config\Source\Mode as ModeModel;

class Mode
{
    private ConfigProvider $configProvider;

    public function __construct(ConfigProvider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    public function getMode1(): bool
    {
        return $this->configProvider->getIsEnabled() && $this->configProvider->getMode() == ModeModel::MODE_VALUE_1;
    }

    public function getMode2(): bool
    {
        return $this->configProvider->getIsEnabled() && $this->configProvider->getMode() == ModeModel::MODE_VALUE_2;
    }

    public function getMode3(): bool
    {
        return $this->configProvider->getIsEnabled() && $this->configProvider->getMode() == ModeModel::MODE_VALUE_3;
    }
}
