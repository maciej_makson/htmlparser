<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Console;

use Alpacode\HtmlParser\Model\CMS\Pages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParsePages extends Command
{
    const COMMAND = 'alpacode:htmlparser:parse:pages';
    const DESCRIPTION = 'Modify content of all CMS Pages';

    private Pages $pages;

    public function __construct(Pages $pages, string $name = null)
    {
        $this->pages = $pages;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName(self::COMMAND);
        $this->setDescription(self::DESCRIPTION);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->pages->execute();
    }
}
