<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Console;

use Alpacode\HtmlParser\Model\CMS\Blocks;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseBlocks extends Command
{
    const COMMAND = 'alpacode:htmlparser:parse:blocks';
    const DESCRIPTION = 'Modify content of all CMS Blocks';

    private Blocks $blocks;

    public function __construct(Blocks $blocks, string $name = null)
    {
        $this->blocks = $blocks;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName(self::COMMAND);
        $this->setDescription(self::DESCRIPTION);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->blocks->execute();
    }
}
