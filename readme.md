# Alpacode HtmlParser

----

Module to adding to html tags attributes with values for CMS pages, blocks and full page

###Magento compatibility

Module compatible with PHP 8.1 and Magento 2.4.5

---------
###Instalation

1. Paste module to app/code 
2. Run `bin/magento setup:upgrade`
-----------------
### How to use

####Enabling module

1. PA -> Stores -> Configuration -> Alpacode -> Html Parser
2. To turn on select "Yes" in "Is enabled"
---
####Select mode

`Mode 1:` Changes the value of saved HTML in the database for cms page/block  
`Mode 2:` Changes the value of the displayed HTML displayed on the storefront when the page is loaded  
`Mode 3:` Changes the value of the displayed HTML for the entire page (all blocks/page together)

####HTML tag and attribute
1. `Tag` - to which html tag add attributes
2. `Attribute` - name of adding attribute to html tag 
3. `Value` - value of adding attribute to html tag
