<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Observer;

use Alpacode\HtmlParser\Model\Parser;
use Alpacode\HtmlParser\Model\Voters\Mode;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CmsRender implements ObserverInterface
{
    const PAGE = 'page';

    private Parser $parser;

    private Mode $mode;

    public function __construct(Parser $parser, Mode $mode)
    {
        $this->parser = $parser;
        $this->mode = $mode;
    }

    public function execute(Observer $observer): self
    {
        if (!$this->mode->getMode2()) {
            return $this;
        }

        $page = $observer->getData(self::PAGE);
        $page->setContent($this->parser->parse($page->getContent()));

        return $this;
    }
}
