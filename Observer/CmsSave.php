<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Observer;

use Alpacode\HtmlParser\Model\Parser;
use Alpacode\HtmlParser\Model\Voters\Mode;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CmsSave implements ObserverInterface
{
    const DATA_OBJECT = 'data_object';

    private Parser $parser;

    private Mode $mode;

    public function __construct(Parser $parser, Mode $mode)
    {
        $this->parser = $parser;
        $this->mode = $mode;
    }

    public function execute(Observer $observer): self
    {
        if (!$this->mode->getMode1()) {
            return $this;
        }
        $page = $observer->getEvent()->getData(self::DATA_OBJECT);
        $page->setContent($this->parser->parse($page->getContent()));

        return $this;
    }
}
