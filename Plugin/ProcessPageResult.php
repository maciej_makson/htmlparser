<?php
/**
 * @copyright Copyright © 2023 Alpacode | www.alpacode.com. All rights reserved.
 */
declare(strict_types=1);

namespace Alpacode\HtmlParser\Plugin;

use Alpacode\HtmlParser\Model\Parser;
use Alpacode\HtmlParser\Model\Voters\Mode;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class ProcessPageResult
{
    private Mode $mode;

    private Parser $parser;

    public function __construct(Mode $mode, Parser $parser)
    {
        $this->mode = $mode;
        $this->parser = $parser;
    }

    public function aroundRenderResult(ResultInterface $subject, \Closure $proceed, ResponseInterface $response)
    {
        $result = $proceed($response);

        if (!$this->mode->getMode3()) {
            return $result;
        }

        $output = $response->getBody();
        $response->setBody($this->parser->parse($output));

        return $result;
    }
}
